# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp
    ((λp.(pz))(λq.(w(λw.((((wq)z)p))))))

2. λp.pq λp.qp
    (λp.((pq)(λp.(qp))))


## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q
    s is bound
    q is bound
    z is free
    
2. (λs. s z) λq. w λw. w q z s
    s is bound 
    z is free always
        
3. (λs.s) (λq.qs)
    s  is bound
    q  is bound 
    s' is free 

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
    z  is bound 
    s  is bound
    q  is bound
    q' is bound
    
## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
    ([(λq.qq)-->z]z)(λs.sa) = (λq.qq)(λs.sa)
    ([(λq.qq)-->q]qq)       = (λs.sa)(λs'.s'a')
    ([(λs'.s'a')-->s]sa)    = (λs'.s'a')a
    [a-->s']s'a'            = aa'
    
2. (λz.z) (λz.z z) (λz.z q)
    ([λz.zz]-->z)zz (λz.zq) = (λz.zz)(λz.zq)
    ([λz.zz]-->z)zz         = (λz.zq)(λz'.z'q')
    ([λz.zq]-->z)zq         = (λz'.z'q')q
    ([q]-->z')z'q'          = qq'

3. (λs.λq.s q q) (λa.a) b
    [(λa.a)-->s]λq.sqq = (λq.(λa.a)aa)b
    [(b)-->q](λa.a)qq  = (λa.a)bb'
    [(bb)->a]a         = bb

4. (λs.λq.s q q) (λq.q) q
    [λq.q-->s]λq.sqq = (λq.(λq.q)qq)q
    [q-->q](λq.q)qq  = (λq'.q')q'q'
    q'q'-->q'        = q''q''

5. ((λs.s s) (λq.q)) (λq.q)
    ([λq.q-->s]ss)  = (λq'.q')(λq.q)
    ([λq'.q'-->q]q) = (λq'.q')(λq.q)
    ([λq.q-->q']q') = λq.q

## Question 4

1. Write the truth table for the or operator below.
         
    p | q |
   -------|----
    T   T | T
    T   F | T
    F   T | T
    F   F | F

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

TT: (λp.λq.p p q) (λa.λb.a) (λa.λb.a)
    ([(λa.λb.a) -> p]λq.p p q)(λa.λb.a)      = (λq.(λa.λb.a)(λa.λb.a)q)(λa.λb.a)
    [(λa.λb.a) -> q](λq.(λa.λb.a)(λa.λb.a)q) = (λa.λb.a)(λa.λb.a)(λa.λb.a)
    ([(λa.λb.a) -> a]λb.a)(λa.λb.a)          = (λb.(λa.λb.a))(λa.λb.a)
    [(λa.λb.a) -> b](λa.λb.a)                = (λa.λb.a) = TRUE

TF: (λp.λq.p p q)
    ([T -> p]λq.p p q)F = (λq.T T q)F
    [F -> q]T T q       = T T F = TRUE

FT: (λp.λq.p p q)
    ([F -> p]λq.p p q)T = (λq.F F q) T
    [T -> q]F F q       = F F T = TRUE

FF: (λp.λq.p p q) 
    ([F -> p]λq.p p q)F = (λq.F F q) F
    [F -> q]F F q       = F F F = FALSE



## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.
    NOT = λp.λa.λb.p b a where b and a are true and falase
    The not operator works by pretty much having a contractive true false statement. In other words it says IF true make false or IF false make true. It essentally uses to if statements in it. It is driveved in reguards to if statments.





## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.